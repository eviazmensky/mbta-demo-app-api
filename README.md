This application is an api endpoint that calls an mbta endpoint which returns a 
csv file which is then formatted into a json object

application require php 5.5 or higher and composer package manager to be installed.

To install this application, browse to the folder you cloned the repo into and run
`php composer.phar install`

to run the application, run `php composer.phar start`
