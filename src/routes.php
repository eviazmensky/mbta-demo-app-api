<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->get('/api/schedules', function(Request $request, Response $response, array $args) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://developer.mbta.com/lib/gtrtfs/Departures.csv');
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $raw_data = curl_exec($ch);
    $array = array_map("str_getcsv", explode("\n", $raw_data));
    $schedule_headers = array_shift($array);
    $schedule_data = [];
    foreach ($array as $key => $schedule) {
        foreach ($schedule_headers as $field_key => $schedule_field){
            $schedule_data[$key][$schedule_field] = $schedule[$field_key];
        }
    }
    array_pop($schedule_data);
    $json = json_encode($schedule_data);
    curl_close($ch);
    
    return $response->getBody()->write($json);
});

